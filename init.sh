#!/bin/bash

sudo apt install ansible vim -y
ansible-galaxy install -r requirements.yml
ansible-galaxy collection install community.general
ansible-playbook main.yaml -K
